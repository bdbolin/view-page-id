<?php
/*
   Plugin Name: View Page ID
   Plugin URI:
   description: Display the Page ID in the Admin Bar
   Version: 0.3
   Author: Brandon Bolin
   Author URI: http://www.flip-9.com
   License: GPL2
*/

add_action('wp_before_admin_bar_render', 'f9_change_admin_bar',999);

function f9_change_admin_bar() {

    if ( ! is_admin() ) {

    global $wp_admin_bar;
    $wp_admin_bar->get_node('edit');

    $postID = get_the_ID();
    if (is_single()):
      $post = 'Post';
    elseif (is_page()):
      $post = 'Page';
    endif;

    $args = array(
        'id' => 'edit',
        'title' => 'Edit ' . $post . ' (ID: ' . $postID . ')',
       'href' => get_edit_post_link(),
    );

    $wp_admin_bar->add_node($args);

    }
}
?>
